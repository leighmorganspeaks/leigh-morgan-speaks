Leigh Morgan is a global executive and independent director whose mission is to build and scale high performing, purpose-based organizations that change the world for the better.

Weebsite: https://www.linkedin.com/in/leigh-morgan-speaks/
